<!DOCTYPE html> 
<html lang="{{ config('app.locale') }}"> 
<head> 
	<meta charset="utf-8"> 
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<meta name="csrf-token" content="{{ csrf_token() }}"> 

	<title>@yield('title') KMA Reviews</title> 
	<link href="{{ asset('css/app.css') }}" rel="stylesheet"> 

	<script src='https://cdn.firebase.com/js/client/2.2.1/firebase.js'></script>
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>
    <script src="https://www.gstatic.com/firebasejs/live/3.1/firebase.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.1.2/firebase-app.js"></script>
	<script src="https://www.gstatic.com/firebasejs/4.1.2/firebase-auth.js"></script>
	<script src="https://www.gstatic.com/firebasejs/4.1.2/firebase-database.js"></script>
	<script src="https://www.gstatic.com/firebasejs/4.1.2/firebase-messaging.js"></script>

  

</head> 

<body> 
	<header> 
		@include('partials.nav')
	</header> 


	@yield('content') 


	<script src="{{ asset('js/app.js') }}"></script> 

	@stack('scripts')


</body> 
</html>