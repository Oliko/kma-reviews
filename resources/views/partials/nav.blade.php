<nav class="navbar navbar-custom">
    <div class="container">    
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">KMA Reviews</a>
            </div>
            <ul class="nav navbar-nav">
                
                @if (\App\Lib\Auth::guest())
                <li><a href="/login">Увійти</a></li>
                <li><a href="/register">Зареєструватись</a></li>
                @else
                <li><a href="/profile">Профіль</a></li>
                <li><a href="/logout">Вийти</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>