@extends('layouts.main')

@section('content')
<div id="login">
	
    <div class="container">
       <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="margin-top: 150px;">
              <div class="panel-heading">Вхід</div>
	            <div class="panel-body">

			   	 	<form @submit.prevent="login" class="form-horizontal" role="form">

			       		<div class="form-group">

				    	    <label for="email" class="col-md-4 control-label">Пошта</label>

				            <div class="col-md-6">
				                <input id="email" type="email" class="form-control" v-model="form.email"  autofocus>
				            </div>
				        </div>

				        <div class="form-group">
				            <label for="password" class="col-md-4 control-label">Пароль</label>

				            <div class="col-md-6">
				                <input id="password" type="password" class="form-control" v-model="form.password" >
				            </div>
				        </div>

					    <div class="form-group">
					        <div class="col-md-8 col-md-offset-4">
					            <button type="submit" class="btn">
					                Увійти
					            </button>
					        </div>
					    </div>
	   				 </form> 
	   			</div>
		    </div>
	    </div>
		</div>
 </div>
</div>

@endsection

@push('scripts')

	<script src="{{ asset('js/login.js') }}"></script> 

@endpush
