@extends('layouts.main')

@section('content')
<div id="register">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">Реєстрація</div>
                        <div class="panel-body">
                            <form  method="POST" action="/newUser" id="newUser">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Ім'я</label>
                                    <input type="text" class="form-control" id="name" name="name" required autofocus>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>Пошта</label>
                                    <input type="email" class="form-control" name="email" id="email" required>
                                    @if ($errors->has('email'))
                                    <div class="alert alert-danger">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Пароль</label>
                                    <input type="password" class="form-control" name="password" id="password" required>
                                    @if ($errors->has('password'))
                                    <div class="alert alert-danger">{{ $errors->first('password') }}</div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Роль</label>
                                    <select name="role" id="role" class="form-control">
                                        <option value="student">Студент</option>
                                        <option value="lector">Викладач</option>
                                    </select>
                                </div>

                                <div class="form-group">                                    
                                    <label>Ваш факультет</label>
                                    <select class="form-control" id="faculty" name="faculty">
                                        @foreach ($faculties as $faculty)
                                            <option value="{{ $faculty->name }}">{{ $faculty->name }}</option>
                                        @endforeach
                                    </select>
                                </div> 

                                <div class="form-group">
                                    <label>Роки перебування в НаУКМА</label>
                                    <p>З якого року :
                                    <input type="text" class="form-control" pattern="[0-9]{4}" placeholder="2010" min="1991-09-01" id="yearOfStudy" name="yearOfStudy" required></p>
                                    <p>По який рік :
                                    <input type="text" class="form-control" pattern="[0-9]{4}" placeholder="2010" max="2021-06-31" id="yearOfGraduation" name="yearOfGraduation"></p>
                                </div>  

                        
                                <input type="hidden" name="date" id="date">

                                <button id="btn" class="btn">Зареєструватись</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>            
    </div>         
</div>
@endsection

@push('scripts')

    <script src="{{ asset('js/register.js') }}"></script> 

@endpush