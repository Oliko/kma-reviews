@extends('layouts.main')

@section('content')
<div id="profile">
    <div class="container">
	
		<div class="heading">Редагування профілю</div>	
		<hr>

		<div class="rs">

			<div class="col-md-6 col-sm-6 col-lg-offset-3">
				<form action="/editProfile" type="POST" id="editProfile">

					<div class="name">
						<div class="form-group">
							<label>Ваше ім'я</label>
							<input type="text" id="name" name="name" class="form-control" value="{{ $user->name }}">
						</div>
					</div>
					
					<div class="email">
						<div class="form-group">
							<label>Ваша пошта</label>
							<input type="text" id="email" name="email" class="form-control" value="{{ $user->email }}">
						</div>						
					</div>

					<div class="faculty">
						<label>Ваш факультет</label>
						<div class="form-group">

						    <select class="form-control" id="faculty" name="faculty" value="{{ $user->faculty }}" >
									<option value="{{ $user->faculty }}">{{ $user->faculty }}"</option>
								@foreach ($faculties as $faculty)
						        	<option value="{{ $faculty->name }}">{{ $faculty->name }}</option>
								@endforeach
						    </select>
						</div>						
					</div>

					<div class="studing">
                        <label>Роки перебування в НаУКМА</label>
                        	<div class="form-group">
                                <p>З якого року :
                                <input type="text" class="form-control" pattern="[0-9]{4}" placeholder="2010" min="1991-09-01" id="yearOfStudy" name="yearOfStudy" required value="{{ $user->yearOfStudy }}"></p>
                                <p>По який рік :
                                <input type="text" class="form-control" pattern="[0-9]{4}" placeholder="2010" max="2021-06-31" id="yearOfGraduation" name="yearOfGraduation" value="{{ $user->yearOfGraduation }}"></p>
                            </div>  
					</div>



					<input type="hidden" name="date" value="{{ $user->registerDate }}">
					<input type="hidden" name="role" value="{{ $user->role }}">

					<button class="btn">
		                Зберегти
		            </button>


				</form>
			</div>

		</div>

		

					            

    </div>
</div>
@endsection

@push('scripts')
	<script src="{{ asset('js/profile.js') }}"></script> 
@endpush