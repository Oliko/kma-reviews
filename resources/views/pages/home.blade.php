@extends('layouts.main')

@section('content')
<div id="home">

	<div class="container">    
		
		<div class="homeHeader">
    		<div class="col-md-10 col-md-offset-1">	
				<img src="/images/kma.jpg" alt="">
			</div>					
		</div>	

		<div class="about col-md-8 col-md-offset-2 text-center">
			Ласкаво просимо на наш сайт! <br>
			У нас ви можете дізнатись багато нового та цікавого: які предмети є найважчими або найбільш пізнавальними, хто з викладачів найсуворіший або найбільш лояльний. <br>
			Для того, щоб ви мали змогу читати відвуки інших людей та залишати свої власні (за винятком викладачів), ви маєте зареєструватись та/або увійти в систему.
			
		</div>

<!-- 			<div class="sections row clearfix">

				<div class="col-md-4 col-sm-6">
					<div class="section">
						<img src="/images/register2.png" alt="">
						<div class="text">
							Реєструйся, залишай свої відкуги та читай інші!
						</div>
					</div>					
				</div>

				<div class="col-md-4 col-sm-6">
					<div class="section">
						<img src="/images/course2.png" alt="">
						<div class="text">
							Дізнавайся які дисципліни найцікавіші!
						</div>
					</div>					
				</div>

				<div class="col-md-4 col-sm-6">
					<div class="section">
						<img src="/images/teacher2.png" alt="">
						<div class="text">
							Дізнавайся нового про викладачів!
						</div>
					</div>					
				</div>

			</div> -->
	

	</div>

	<div class="faculties">
		<div class="container">
			
			<div class="heading">	
				Факультети
			</div>	

			<div class="sections row clearfix">

				@foreach ($faculties as $id => $faculty)
					<div class="col-md-4 col-sm-6">
						<a href="{{ route('faculty', $id) }}">
							<div class="faculty">					
								<img src="{{ $faculty->img }}" alt="">
								<div class="name">{{ $faculty->name }}</div>
							</div>
						</a>
					</div>
				@endforeach

			</div>
		</div>		
	</div>


	
</div>


@stop