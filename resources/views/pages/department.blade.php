@extends('layouts.main')

@section('content')
<div id="department">

	<div class="container">    
		<div class="heading">{{ $department->name }}</div>	
		<hr>
		<div class="about">
			<div class="row clearfix">					
				<div class="text text-center col-sm-8 col-lg-offset-2">		

				</div>
			</div>
		</div>
	</div>

	<div class="lectors">
		<div class="container">			
			<div class="heading">	
				Викладачі
			</div>	
			<div class="sections row clearfix">
				@foreach ($lectors as $lectorId => $lector)
					<div class="col-md-3 col-sm-6">
						<a href="{{ route('lector', [$departmentId, $lectorId]) }}">
							<div class="lector">
								<img src="{{ $lector->img }}" alt="">				
								<div class="name">
									{{ $lector->name }}
								</div>
							</div>
						</a>
					</div>		
				@endforeach
			</div>
		</div>		
	</div>

	<div class="container">
		<div class="courses">			
			<div class="heading">	
				Дисципліни
			</div>	
			<hr>
	
			<div class="sections row clearfix">
				@foreach ($courses as $courseId => $course)
					<div class="col-md-2 col-sm-6">
						<a href="{{ route('course', [$departmentId, $courseId]) }}">
							<div class="course">			
								<div class="name">
									{{ $course->name }}
								</div>
							</div>
						</a>
					</div>		
				@endforeach	

			</div>

		</div>
	</div>

</div>
@stop