@extends('layouts.main')

@section('content')
<div id="course">
	<div class="container">    

		<div class="heading">{{ $course->name }}</div>	
		<hr>	

		<div class="about">
			<div class="info text-center">
				{{ $course->credits }} кредитів; {{ $course->hours }} годин;
			</div>
			<div class="row clearfix">					
				<div class="text text-center col-sm-8 col-lg-offset-2">{{ $course->about }}</div>
			</div>
		</div>

		<div class="lectors">	
				<div class="heading">	
					Викладачі
				</div>	
				<hr>	
				<div class="sections row clearfix">
					@if (count($lectors) == 0) 
						<div class="comment text-center">Викладачі не вказані</div>
					@else
						@foreach ($lectors as $lector)
							<div class="col-md-3 col-sm-6">
									<div class="lector">
										<img src="{{ $lector->img }}" alt="">				
										<div class="name">
											{{ $lector->name }}
										</div>
									</div>
							</div>		
						@endforeach
					@endif
				</div>
		</div>

		<div class="reviews">
				
			<div class="heading">Відгуки</div>	
			<hr>
				
			@if (\App\Lib\Auth::guest())
				<div class="comment">Щоб переглянути відгуки, увійдіть в систему, будь ласка</div>
			@else

				<div class="sections row clearfix">
					@if (count($reviews) == 0) 
						<div class="comment">Відгуків поки немає :(<br>Але ви можете залишити свій власний!</div>	
					@else
						@foreach ($reviews as $review)
							<div class="col-md-8 col-sm-8 col-lg-offset-2">
								<div class="review">

									<div class="head row clearfix">
										<div class="col-sm-8">
											<div class="user">
												{{ $review->user->name }} ({{ $review->user->faculty }})
											</div>	
										</div>
										<div class="col-sm-4">
											<div class="date text-right">												
												{{ $review->date }}
											</div>	
										</div>
									</div>				

									<div class="rate">
										<div class="difficulty">
	                            			<label class="control-label">Важкість дисципліни : </label>	
											{{ $review->difficulty }}							
										</div>	
										<div class="interest">
	                            			<label class="control-label">Цікавість дисципліни : </label>
											{{ $review->interest }}											
										</div>		
										<div class="usefulness">
	                            			<label class="control-label">Корисність дисципліни : </label>
											{{ $review->usefulness }}	
										</div>
									</div>
									
									<div class="text">
										{{ $review->text }}
									</div>
								</div>
							</div>	


						@endforeach
					@endif
				</div>

				@if (\App\Lib\Auth::user()->role == 'student')
	   	 			<div class="newReview col-md-8 col-sm-8 col-lg-offset-2">										
						<form action="/addCourseReview" type="POST" id="addReview">

							<div class="form-group">
								<label>Оцініть наскільки важкою є дисципліна : </label>
								<select class="form-control" id="difficulty" name="difficulty">
								    <option value="1">1</option>
								    <option value="1.5">1.5</option>
								    <option value="2">2</option>
								    <option value="2.5">2.5</option>
								    <option value="3">3</option>
								    <option value="3.5">3.5</option>
								    <option value="4">4</option>
								    <option value="4.5">4.5</option>
								    <option value="5">5</option>
								</select>
							</div>

							<div class="form-group">
								<label>Оцініть наскільки цікавою є дисципліна : </label>
								<select class="form-control" id="interest" name="interest">
								    <option value="1">1</option>
								    <option value="1.5">1.5</option>
								    <option value="2">2</option>
								    <option value="2.5">2.5</option>
								    <option value="3">3</option>
								    <option value="3.5">3.5</option>
								    <option value="4">4</option>
								    <option value="4.5">4.5</option>
								    <option value="5">5</option>
								</select>
							</div>

							<div class="form-group">
								<label>Оцініть наскільки корисною є дисципліна : </label>
								<select class="form-control" id="usefulness" name="usefulness">
								    <option value="1">1</option>
								    <option value="1.5">1.5</option>
								    <option value="2">2</option>
								    <option value="2.5">2.5</option>
								    <option value="3">3</option>
								    <option value="3.5">3.5</option>
								    <option value="4">4</option>
								    <option value="4.5">4.5</option>
								    <option value="5">5</option>
								</select>
							</div>

							<div class="form-group">
								<label>Відгук</label>
						   		<textarea class="form-control" id="text" rows="5" name="text" required></textarea>
							</div>


<!-- 							<div class="form-check">
							    <label class="form-check-label">
							      <input type="checkbox" class="form-check-input" name="anonymity">
							      Анонімно
							    </label>
							</div> -->

							<button class="btn btn-success">Написати</button>

							<input type="hidden" name="course_id" value="{{ $courseId }}">
							<input type="hidden" name="date">

						</form>					
					</div>	
				@endif					
			
			@endif

		</div>



</div>
@stop

@push('scripts')
	<script src="{{ asset('js/courseReviews.js') }}"></script> 
@endpush