@extends('layouts.main')

@section('content')
<div id="lector">
	<div class="container">    

		<div class="heading">{{ $lector->name }}</div>	
		<hr>	

		<div class="about">
			<div class="row clearfix">					
				<div class="text-center col-sm-8 col-lg-offset-2">{{ $lector->about }}</div>
			</div>
		</div>

		<div class="courses">	
			<div class="heading">	
				Дисципліни
			</div>	
			<hr>	
			<div class="sections row clearfix">
				@if (count($courses) == 0) 
					<div class="comment text-center">Дисципліни не вказані</div>
				@else
					@foreach ($courses as $course)
						<div class="col-md-3 col-sm-6">
								<div class="course">			
									<div class="name">
										{{ $course->name }}
									</div>
								</div>
						</div>		
					@endforeach
				@endif
			</div>

		</div>

		<div class="reviews">
				
			<div class="heading">Відгуки</div>	
			<hr>
				
			@if (\App\Lib\Auth::guest())
				<div class="comment">Щоб переглянути відгуки, увійдіть в систему, будь ласка</div>
			@else

				<div class="sections row clearfix" id="reviews">
					@if (count($reviews) == 0) 
						<div class="comment">Відгуків поки немає :(<br>Але ви можете залишити свій власний!</div>	
					@else
						@foreach ($reviews as $review)
							<div class="col-md-8 col-sm-8 col-lg-offset-2">
								<div class="review">
									
									<div class="head row clearfix">
										<div class="col-sm-8">
											<div class="user">
												{{ $review->user->name }} ({{ $review->user->faculty }}, {{ $review->user->yearOfStudy }} - {{ $review->user->yearOfGraduation }})
											</div>	
										</div>
										<div class="col-sm-4">
											<div class="date text-right">												
												{{ $review->date }}
											</div>	
										</div>
									</div>
									
									<div class="rate">
										<div class="competency">
	                            			<label class="control-label">Обізнаність викладача:</label>	
											{{ $review->competency }}							
										</div>	
										<div class="qualityOfTeaching">
	                            			<label class="control-label">Якість викладання:</label>
											{{ $review->qualityOfTeaching }}											
										</div>		
										<div class="justiceAssessment">
	                            			<label class="control-label">Справедливість оцінювання:</label>
											{{ $review->justiceAssessment }}	
										</div>
									</div>
									
									<div class="text">
										{{ $review->text }}
									</div>
								</div>
							</div>	


						@endforeach
					@endif
				</div>

				@if (\App\Lib\Auth::user()->role == 'student')
	   	 			<div class="newReview col-md-8 col-sm-8 col-lg-offset-2">										
						<form action="/addLectorReview" type="POST" id="addReview">

							<div class="form-group">
								<label>Оцініть обізнаність викладача : </label>
								<select class="form-control" id="competency" name="competency">
								    <option value="1">1</option>
								    <option value="1.5">1.5</option>
								    <option value="2">2</option>
								    <option value="2.5">2.5</option>
								    <option value="3">3</option>
								    <option value="3.5">3.5</option>
								    <option value="4">4</option>
								    <option value="4.5">4.5</option>
								    <option value="5">5</option>
								</select>
							</div>

							<div class="form-group">
								<label>Оцініть якість викладання : </label>
								<select class="form-control" id="qualityOfTeaching" name="qualityOfTeaching">
								    <option value="1">1</option>
								    <option value="1.5">1.5</option>
								    <option value="2">2</option>
								    <option value="2.5">2.5</option>
								    <option value="3">3</option>
								    <option value="3.5">3.5</option>
								    <option value="4">4</option>
								    <option value="4.5">4.5</option>
								    <option value="5">5</option>
								</select>
							</div>

							<div class="form-group">
								<label>Оцініть справедливість оцінювання студентів : </label>
								<select class="form-control" id="justiceAssessment" name="justiceAssessment">
								    <option value="1">1</option>
								    <option value="1.5">1.5</option>
								    <option value="2">2</option>
								    <option value="2.5">2.5</option>
								    <option value="3">3</option>
								    <option value="3.5">3.5</option>
								    <option value="4">4</option>
								    <option value="4.5">4.5</option>
								    <option value="5">5</option>
								</select>
							</div>

							<div class="form-group">
								<label>Відгук</label>
						   		<textarea class="form-control" id="text" rows="5" name="text" required></textarea>
							</div>


						<!-- 	<div class="form-check">
							    <label class="form-check-label">
							      <input type="checkbox" class="form-check-input" name="anonymity">
							      Анонімно
							    </label>
							</div> -->

							<button class="btn btn-success">Написати</button>

							<input type="hidden" name="lector_id" value="{{ $lectorId }}">
							<input type="hidden" name="date">

						</form>					
					</div>
				@endif						
			
			@endif

		</div>


	</div>
</div>
@stop

@push('scripts')
<script src="{{ asset('js/lectorReviews.js') }}"></script> 
@endpush