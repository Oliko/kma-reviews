@extends('layouts.main')

@section('content')
<div id="faculty">

	<div class="container">    
		<div class="heading">{{ $faculty->name }}</div>	
		<hr>
		
		<div class="about">
			<div class="row clearfix">					
				<div class="text text-center col-sm-8 col-lg-offset-2">{{ $faculty->about }}</div>
			</div>
		</div>
	</div>

	<div class="departments">
		<div class="container">
			
			<div class="heading">	
				Кафедри
			</div>	

			<div class="sections row clearfix">
				@foreach ($faculty->departments as $departmentId => $department)
					<div class="col-md-3 col-sm-6">
						<a href="{{ route('department', [$facultyId, $departmentId]) }}">
							<div class="department">					
								<div class="name">
									{{ $department->name }}
								</div>
							</div>
						</a>
					</div>
				@endforeach				
			</div>
			
		</div>		
	</div>


</div>
@stop