const login = new Vue({
    el: '#login',

    data: {
        form: {
            email: '',
            password: '',
        },
    },

    methods: {
        login() {

            axios.post('https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyBFzal-2o1D89Fmi8VKP0Zory0C5wvxXvs', this.form)
                .then(response => { 
                   this.authUser(response.data.email);
                })
                .catch(error =>alert('error'));
        },
        authUser(email) {
           axios.post('/auth', {email: email})
                .then(response => window.location.replace("/"))
                .catch(error =>alert('error'));
        }
    }, 

});