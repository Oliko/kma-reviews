$("#addReview").submit(function(e) {
    let form = $(this);

    $.ajax({
        type: form.attr('type'),
        url: form.attr('action'),
        data: form.serialize(), 
        success: function(data) {  
            reload();  
            form.find('input').val(' ');
        }
    });
    e.preventDefault(); 
}); 