// alert(window.location.href);
// reload();
$("#addReview").submit(function(e) {
    let form = $(this);

    $.ajax({
        type: form.attr('type'),
        url: form.attr('action'),
        data: form.serialize(), 
        success: function(data) {  
            reload();  
            form.find('input').val(' ');
        }
    });

    e.preventDefault(); 
}); 

function reload() {

   $.ajax({
        type: "GET",
        url: window.location.href,
        success: function(response) {
            let sections = $(response).find('#reviews').html();
            $('#reviews').html(sections);  
        }
    });
}