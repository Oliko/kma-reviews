<?php

use Illuminate\Http\Request;

Route::post('/login', 'Api\FirebaseController@login')->name('login');
Route::post('/addReview', 'Api\LectorsController@addReview')->name('addReview');
// Route::post('add/product', 'Api\ProductsController@add')->name('api.add.product');