<?php

Auth::routes();

Route::get('/', 'HomeController@home')->name('home');
Route::get('/faculty/{id}', 'FacultiesController@faculty')->name('faculty');
Route::get('/faculty/{facultyId}/department/{departmentId}', 'DepartmentsController@department')->name('department');
Route::get('/lector/{departmentId}/{lectorId}', 'LectorsController@lector')->name('lector');
Route::get('/course/{departmentId}/{courseId}', 'CoursesController@course')->name('course');


Route::group(['middleware' => 'guest'], function () {
	Route::get('/login', 'AuthController@login')->name('login');
	Route::post('/auth', 'AuthController@auth')->name('auth');
	Route::get('/register', 'AuthController@register')->name('register');
});


Route::post('/newUser', 'Api\RegisterController@register')->name('newUser');

Route::group(['middleware' => 'auth'], function () {
	Route::get('profile', 'ProfileController@profile')->name('profile');
	Route::get('/logout', 'AuthController@logout')->name('logout');
	Route::post('/addLectorReview', 'Api\LectorsController@addReview')->name('addLectorReview');
	Route::post('/addCourseReview', 'Api\CoursesController@addReview')->name('addCourseReview');
	Route::post('/editProfile', 'Api\ProfileController@edit')->name('editProfile');
});
