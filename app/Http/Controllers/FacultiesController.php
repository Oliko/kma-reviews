<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faculty;

class FacultiesController extends Controller
{
    public function faculty($id) 
    {
    	$faculty = Faculty::table()->get($id);

    	return view('pages.faculty')
			->with('faculty', $faculty)
			->with('facultyId', $id);
    }
}
