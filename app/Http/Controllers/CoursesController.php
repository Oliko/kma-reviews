<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\Lector;
use App\Models\Review;
use App\Models\User;
use App\Models\Faculty;

class CoursesController extends Controller
{
    public function course($departmentId, $courseId) 
    {
    	$course = Course::table()->get("$departmentId.$courseId");

        $lectors = [];
        foreach ($course->teachers as $lector) {
            if ($lector) {
                $lectors[] = Lector::table()->get("$departmentId.$lector"); 
                // $lectorId = $lector;   
                // dd($lectorId);
            }      
        }

        $reviews = Review::table()->get("courses.$courseId") ?? [];       
        foreach ($reviews as $review) {
            $review->user = User::table()->get("$review->userID");
            $userFaculty = $review->user->faculty;
        }

     	return view('pages.course')
            ->with('departmentId', $departmentId)
			->with('course', $course)
			->with('courseId', $courseId)
			->with('lectors', $lectors)
            ->with('reviews', $reviews);
    }
}
