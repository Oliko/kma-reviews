<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\Lector;
use App\Models\Review;
use App\Models\User;
use App\Models\Faculty;

class LectorsController extends Controller
{
    public function lector($departmentId, $lectorId) 
    {    	
    	$lector = Lector::table()->get("$departmentId.$lectorId");

        $courses = [];
        foreach ($lector->courses as $course) {
            if ($course) {
                $courses[] = Course::table()->get("$departmentId.$course");
            }
        }        

        $reviews = Review::table()->get("teachers.$lectorId") ?? []; 
        foreach ($reviews as $review) {
            $review->user = User::table()->get("$review->userID");
            $userFaculty = $review->user->faculty;
        }

    	return view('pages.lector')
            ->with('departmentId', $departmentId)
			->with('lector', $lector)
            ->with('lectorId', $lectorId)
            ->with('courses', $courses)
            ->with('reviews', $reviews);
    }
}