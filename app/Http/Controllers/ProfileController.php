<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faculty;
use App\Lib\Auth;

class ProfileController extends Controller
{
	public function profile()
	{
		$user = Auth::user();

    	$faculties = Faculty::table()->all();
		// $userFaculty = Faculty::table()->get("$user->faculty");

		return view('user.profile')
			->with('user', $user)
			->with('faculties', $faculties);
	}
}
