<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faculty;
use Session;

class HomeController extends Controller
{
    public function home() 
    {
    	//$logged = Session::has('email');

    	$faculties = Faculty::table()->all();

    	return view('pages.home')
    		->with('faculties', $faculties);
    }
}
