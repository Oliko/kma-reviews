<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Lib\Auth;
use App\Models\Faculty;

class AuthController extends Controller
{
    public function login()
    {
    	return view('user.login');
    }
    
    public function auth(Request $request) 
    {
    	$email = $request->email;

    	Session::put('email', $email);
        
    	return Session::get('email');
    }

    public function register()
    {
        $faculties = Faculty::table()->all();

        return view('user.register')
            ->with('faculties', $faculties);
    }


    public function logout()
    {   
        Session::forget('email');
        
        return redirect()->route('home');
    }
}
