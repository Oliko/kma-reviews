<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Review;
use App\Lib\Auth;


class CoursesController extends Controller
{   
    public function addReview(Request $request)
    {

        $date = $request->date ?? date('d-m-Y');

        $newReview = [
            'difficulty' => $request->difficulty,
            'interest' => $request->interest,
            'usefulness' => $request->usefulness,
            'text' => $request->text,
            'anonymity' => $request->anonymity,
            'userID' => Auth::user()->id,
            'date' => $date
    	];

        return $addingReview = Review::table()->insert("courses.$request->course_id", $newReview);
    }

}
