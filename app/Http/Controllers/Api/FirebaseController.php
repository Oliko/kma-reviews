<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Session;

class FirebaseController extends Controller
{
    public function login(Request $request) 
    {
    	$email = $request->email;

    	Session::put('email', $email);

    	return Session::get('email');
    }
}
