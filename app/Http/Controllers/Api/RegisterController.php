<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class RegisterController extends Controller
{
   public function register(Request $request) 
   {
        $date = $request->date ?? date('d-m-Y');

    	$data = [
            'name' => $request->name,
            'email' => $request->email,
            'faculty' => $request->faculty,
            'registerDate' => $date,
            'yearOfStudy' => $request->yearOfStudy,
            'yearOfGraduation' => $request->yearOfGraduation,
            'role' => $request->role,
    	];

        $newUser = User::table()->insert(".", $data);
        return view('user.login');
   }
}


            // 'yearOfStudy' => $request->yearOfStudy,
            // 'yearOfGraduation' => $request->yearOfGraduation,