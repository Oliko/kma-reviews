<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Faculty;
use App\Models\User;
use App\Lib\Auth;

class ProfileController extends Controller
{
    public function edit(Request $request)
    {
    	$id = Auth::user()->id;

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'faculty' => $request->faculty,
            'registerDate' => $request->date,
            'yearOfStudy' => $request->yearOfStudy,
            'yearOfGraduation' => $request->yearOfGraduation,
            'role' => $request->role,
    	];

        return $editing = User::table()->set("$id", $data);
    }
    
}

