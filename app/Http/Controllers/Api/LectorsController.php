<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Review;
use App\Lib\Auth;

class LectorsController extends Controller
{    
    public function addReview(Request $request)
    {

        $date = $request->date ?? date('d-m-Y');

        $newReview = [
            'competency' => $request->competency,
            'justiceAssessment' => $request->justiceAssessment,
            'qualityOfTeaching' => $request->qualityOfTeaching,
            'text' => $request->text,
            'anonymity' => $request->anonymity,
            'userID' => Auth::user()->id,
            'date' => $date
    	];

        return $addingReview = Review::table()->insert("teachers.$request->lector_id", $newReview);
    }

}
