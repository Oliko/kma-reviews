<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\Faculty;
use App\Models\Lector;
use App\Models\Course;

class DepartmentsController extends Controller
{
    public function department($facultyId, $departmentId) 
    {
    	$department = Faculty::table()->get("$facultyId.departments.$departmentId");

    	$lectors = Lector::table()->get($departmentId);
    	$courses = Course::table()->get($departmentId);

    	return view('pages.department')
			->with('department', $department)
            ->with('departmentId', $departmentId)
			->with('lectors', $lectors)
            ->with('courses', $courses);
    }
}
