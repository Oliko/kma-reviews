<?php

namespace App\Http\Middleware;

use Closure;
use App\Lib\Auth;

class FBAuth
{
    public function handle($request, Closure $next)
    {
        if (Auth::guest()) {
            return redirect()->route('login');
        }

        return $next($request);
    }
}
