<?php

namespace App\Http\Middleware;

use Closure;
use App\Lib\Auth;

class Guest
{
    public function handle($request, Closure $next)
    {        
        if (Auth::check()) {
            return redirect()->route('home');
        }

        return $next($request);
    }
}
