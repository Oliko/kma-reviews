<?php

namespace App\Models;

use Firebase\FirebaseLib;

abstract class FirebaseModel
{
    protected $entity;

    public function __construct()
    {
        $url = config('database')['firebase']['url'];
        $token = config('database')['firebase']['token'];

        $this->entity = new FirebaseLib($url, $token);
    }

    public static function table()
    {
        return new static;
    }

    public function get($path)
    {
        $realPath = str_replace('.', '/', $path);
        $query = "{$this->table}/{$realPath}";

        return $this->getResult($query);
    }

    public function all()
    {
        return $this->getResult($this->table);
    }

    public function insert(string $path, array $columns)
    {
        $realPath = str_replace('.', '/', $path);
        $this->entity->push("{$this->table}/{$realPath}", $columns);
    }

    public function set(string $path, $data)
    {
        $realPath = str_replace('.', '/', $path);
        $this->entity->set("{$this->table}/{$realPath}", $data);
    }

    public function delete(string $path)
    {
        $realPath = str_replace('.', '/', $path);
        $this->entity->delete("{$this->table}/{$realPath}");
    }

    protected function getResult($query)
    {
        $result = $this->entity->get($query);

        return json_decode($result);
    }
}
