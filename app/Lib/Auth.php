<?php

namespace App\Lib;
use Illuminate\Http\Request;
use Session;
use App\Models\User;

class Auth 
{
	public static function check()
	{		
		return Session::has('email');
	}

	public static function guest()
	{	
		return !self::check();
	}

	public static function user()
	{
		$email = Session::get('email');
		$users = User::table()->all();
		$authUser = null;

		foreach ($users as $id => $user) {		
            if ($user->email == $email) {
				$authUser = $user;
				$authUser->id = $id;
			}
		}

		return $authUser;
	}
}
