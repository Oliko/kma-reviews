<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'firebase' => [
        'api_key' => 'AIzaSyCLo1fWTv-C6Lr9UjFAQegYmg846eJATGc', // Only used for JS integration
        'auth_domain' => 'tophick-698df.firebaseapp.com', // Only used for JS integration
        'database_url' => 'https://tophick-698df.firebaseio.com',
        'secret' => 'xR352Eojpq50OiScFinAo92CEMV4W1voUD9BxC8H',
        'storage_bucket' => 'tophick-698df.appspot.com', // Only used for JS integration
    ]

];
