# KMA Reviews 

The system allows NaUKMA students to post reviews and evaluate courses and teachers. 
Only registered users can see reviews for some time without leaving their own reviews. After time is up, user should post his own review to have opportunity to read others reviews. 

# How to install 

To edit existing project, clone repo to your computer. 

You have to install Open Server (or another local server). Open its settings, find "Domains" and add there path to project's "public" folder. 

Then you install Laravel. The next thing you should do after installing Laravel is set your application key to a random string. If you installed Laravel via Composer or the Laravel installer, this key has already been set for you by the php artisan key:generate command. 
Next you create ".env" file and set there "api_key" and add these strings:

FIREBASE_URL=https://tophick-698df.firebaseio.com
FIREBASE_TOKEN=xR352Eojpq50OiScFinAo92CEMV4W1voUD9BxC8H

Then you write in console "composer update". So you already can run the application. 

But if you want to work on it, you also need to install Node.js and npm.
 

# Related projects 

[Android App](https://github.com/SanchoPanchos/topchik-team-android) 

[iOS App](https://github.com/IraNikolenko/SurveyKMA_iOS/) 

**Made by Topchik Team**